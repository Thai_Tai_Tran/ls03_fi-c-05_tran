package model.persistence;

public interface IPersistence {

	IAttemptPersistence getAttemptPersistence();
	ILevelPersistence getLevelPersistence();
	ITargetPersistence getTargetPersistence();
	IUserPersistence getUserPersistence();
	
}
