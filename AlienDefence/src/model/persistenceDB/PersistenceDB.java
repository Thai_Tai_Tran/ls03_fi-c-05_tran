package model.persistenceDB;

import model.persistence.IAttemptPersistence;
import model.persistence.ILevelPersistence;
import model.persistence.IPersistence;
import model.persistence.ITargetPersistence;
import model.persistence.IUserPersistence;

public class PersistenceDB implements IPersistence{

	private LevelDB levelDB;
	private UserDB userDB;
	private AttemptDB attemptDB;
	private TargetDB targetDB;

	public PersistenceDB() {
		AccessDB dbAccess = new AccessDB();
		this.levelDB = new LevelDB(dbAccess);
		this.userDB = new UserDB(dbAccess);
		this.attemptDB = new AttemptDB(dbAccess);
		this.targetDB = new TargetDB(dbAccess);
	}

	@Override
	public IAttemptPersistence getAttemptPersistence() {
		return this.attemptDB;
	}

	@Override
	public ILevelPersistence getLevelPersistence() {
		return this.levelDB;
	}

	@Override
	public ITargetPersistence getTargetPersistence() {
		return this.targetDB;
	}

	@Override
	public IUserPersistence getUserPersistence() {
		return this.userDB;
	}
}
