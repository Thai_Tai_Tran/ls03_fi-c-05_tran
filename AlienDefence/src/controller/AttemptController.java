package controller;

import java.util.Vector;

import model.Level;
import model.persistence.IAttemptPersistence;
import model.persistence.IPersistence;

public class AttemptController {

	private IAttemptPersistence attemptPersistence;

	/**
	 * erstellt ein neues Objekt eines AttemptController welches Attemptobjekte in
	 * der übergebenen Datenhaltung persisiert
	 * 
	 * @param alienDefenceModel.getAttemptDB()
	 *            Persistenzklasse der Attemptobjekte
	 */
	public AttemptController(IPersistence alienDefenceModel) {
		this.attemptPersistence = alienDefenceModel.getAttemptPersistence();
	}

	public Vector<Vector<String>> getAllAttemptsPerLevel(Level level, int game_id) {
		return attemptPersistence.getAllAttemptsPerLevel(level, game_id);
	}

	public int getPlayerPosition() {
		return attemptPersistence.getPlayerPosition();
	}

	public void deleteHighscore(int level_id) {
		attemptPersistence.deleteHighscore(level_id);
	}

	/**
	 * calculates points from attempt for highscore TODO create formula here
	 * 
	 * @param level Levelobjekt
	 * @param hitcounter Controllerobjekt das die Treffer und Reaktionszeiten misst
	 * @return points 
	 */
	public int calculatePoints(Level level, HitCounter hitcounter) {
		return -1;
	}
}
